absl-py==2.1.0
aiosignal==1.3.1
astunparse==1.6.3
attrs==23.2.0
Automat==22.10.0
certifi==2024.2.2
charset-normalizer==3.3.2
click==8.1.7
cloudpickle==3.0.0
constantly==23.10.4
coverage==7.5.1
decorator==5.1.1
dm-tree==0.1.8
exceptiongroup==1.2.1
execnet==2.1.1
filelock==3.14.0
flatbuffers==24.3.25
frozenlist==1.4.1
fsspec==2024.5.0
future==1.0.0
gast==0.5.4
google-pasta==0.2.0
grpcio==1.64.0
h5py==3.11.0
hyperlink==21.0.0
idna==3.7
importlib_metadata==7.1.0
incremental==22.10.0
iniconfig==2.0.0
Jinja2==3.1.4
jsonschema==4.22.0
jsonschema-specifications==2023.12.1
keras==3.3.3
libclang==18.1.1
Markdown==3.6
markdown-it-py==3.0.0
MarkupSafe==2.1.5
mdurl==0.1.2
ml-dtypes==0.3.2
mpmath==1.3.0
msgpack==1.0.8
namex==0.0.8
networkx==3.2.1
numpy==1.26.4
opencv-python==4.9.0.80
opencv-python-headless==4.9.0.80
opt-einsum==3.3.0
optree==0.11.0
packaging==24.0
Panda3D==1.10.14
panda3d-gltf==0.13
panda3d-simplepbr==0.12.0
pillow==10.3.0
pluggy==1.5.0
protobuf==4.25.3
psutil==5.9.8
py==1.11.0
py-cpuinfo==9.0.0
pybullet==3.2.6
Pygments==2.18.0
pytest==8.2.1
pytest-benchmark==4.0.0
pytest-cov==5.0.0
pytest-forked==1.6.0
pytest-xdist==3.6.1
PyYAML==6.0.1
ray==2.9.0
referencing==0.35.1
requests==2.32.1
rich==13.7.1
rpds-py==0.18.1
scipy==1.13.0
shapely==2.0.4
six==1.16.0
sympy==1.12
tableprint==0.9.1
tensorboard==2.16.2
tensorboard-data-server==0.7.2
tensorflow==2.16.1
tensorflow-io-gcs-filesystem==0.37.0
tensorflow-probability==0.24.0
termcolor==2.4.0
tomli==2.0.1
torch==2.2.2
torchvision==0.17.2
trimesh==4.4.0
Twisted==24.3.0
typing_extensions==4.11.0
urllib3==2.2.1
wcwidth==0.2.13
Werkzeug==3.0.3
wrapt==1.16.0
yattag==1.15.2
zipp==3.18.2
zope.interface==6.4
